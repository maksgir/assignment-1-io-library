

section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; начало строки кладется в rdi
string_length:
    xor rax, rax
    .loop:
      cmp byte [rdi + rax], 0
      je .end
      inc rax
      jmp .loop
    .end:
      ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; указатель на строку кладется в rdi
print_string:
    push rsi
    call string_length
    pop rsi
    mov rsi, rdi
    mov rdx, rax
    mov rax, 1 
    mov rdi, 1

    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, 1
    push rdi
    
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1

    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char
    

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r9, rsp
    push 0
    mov rax, rdi
    mov r8, 10
    .loop:
      xor rdx, rdx
      div r8
      add rdx, '0'
      
      dec rsp
      mov [rsp], dl ; lower byte of rdx

      cmp rax, 0
      jne .loop
    .end:
      mov r8, rdi
      mov rdi, rsp
      push r9
      call print_string
      pop r9
      mov rdi, r8
      mov rsp, r9
      ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .print ; if rdi >= 0 then print it
	       ; else ...
    mov r8, rdi
    mov rdi, '-'
    push r8
    call print_char
    pop r8
    neg r8
    mov rdi, r8 
    .print:
      jmp print_uint 
      

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    mov r8, rax ; r8 - length of first string
    push rdi
    mov rdi, rsi
    call string_length
    pop rdi

    cmp r8, rax ; rax contains len of second string
    jne .not_equals

    xor rax, rax
    .loop:
      mov r9b, byte [rdi + rax]
      cmp byte [rsi + rax], r9b 
      jne .not_equals
      cmp r8, rax
      je .equals
      inc rax
      jmp .loop
    .equals:
      mov rax, 1
      ret
    .not_equals:
      xor rax, rax
      ret
    

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
    xor rcx, rcx
    .loop:
       push rcx
       push r8
       push r9
       call read_char
       pop r9
       pop r8
       pop rcx
       
       cmp rax, ' '
       je .check_to_stop_read
       cmp rax, `\t`
       je .check_to_stop_read
       cmp rax, `\n`
       je .check_to_stop_read
       cmp rax, 0
       je .add_null_and_end
       cmp rax, 0x3 ; ctrl-D ?
       je .add_null_and_end
       
       mov byte [r8 + rcx], al
       inc rcx
       cmp rcx, r9
       je .end
       jmp .loop
       
    .check_to_stop_read:
      cmp rcx, 0
      je .loop
      
    .add_null_and_end:
       mov byte [r8 + rcx], 0
       mov rax, r8
       mov rdx, rcx
       ret
    .end:
       xor rax, rax
       ret
 

 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r9, 10
    .loop:
      xor r8, r8
      mov r8b, byte [rdi + rcx]
      sub r8, '0'
      cmp r8, 9
      jg .process_ret
      cmp r8, 0
      jl .process_ret
      mul r9
      add al, r8b
      inc rcx
      jmp .loop
    .process_ret:
      cmp rcx, 0
      je .bad_parse
      mov rdx, rcx
      ret
    .bad_parse:
      xor rdx, rdx
      ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor r8, r8
    xor r10, r10
    mov r8b, byte [rdi]
    cmp r8, '-'
    jne .process
    inc rdi
    mov r10, 1
    .process:
      call parse_uint
      cmp rdx, 0
      je .bad_parse
      cmp r10, 1
      je .process_minus
      ret
    .process_minus:
      inc rdx
      neg rax
      ret
    .bad_parse:
      ret
    
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push rdx
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    pop rdx
    cmp rax, rdx
    ja .not_equals
    
    xor rcx, rcx
    .loop:
      mov r8b, byte [rdi + rcx]
      mov byte [rsi + rcx], r8b
      cmp rcx, rax
      je .end
      inc rcx
      jmp .loop
      
    .end:
      mov rax, rcx
      ret
    .not_equals:
      xor rax, rax
      ret




